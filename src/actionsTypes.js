export const SET_FIELDS = 'SET_FIELDS';
export const SET_DELAY = 'SET_DELAY';
export const SET_NAME = 'SET_NAME';
export const START_GAME = 'START_GAME';
export const STOP_GAME = 'STOP_GAME';
export const SET_MESSAGE = 'SET_MESSAGE';
