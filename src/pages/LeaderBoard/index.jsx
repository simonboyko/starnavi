import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import LeadersList from '../../components/LeadersList';


const Wrapper = styled.div`
  min-height: 100vh;
  max-width: 400px;
  margin: 70px auto 50px;
`;

const H1 = styled.h1`
  margin: 0 0 10px;
  font-size: 34px;
  color: #888;
  font-family: 'sans-serif';
`;

const BackLink = styled(Link)`
  display: inline-block;
  color: #888;
  margin-bottom: 30px;
  
  &:hover {
    text-decoration: none;
  }
`;

export default function LeaderBoard() {
  return (
    <Wrapper>
      <H1>Leader board!</H1>
      <BackLink to="/">Back to game</BackLink>
      <LeadersList />
    </Wrapper>
  );
}
