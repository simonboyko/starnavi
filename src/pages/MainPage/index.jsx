import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import PlayingField from '../../components/PlayingField';
import DifficultySelect from '../../components/DifficultySelect';
import NameInput from '../../components/NameInput';
import PlayButton from '../../components/PlayButton';
import Message from '../../components/Message';


const Wrapper = styled.div`
  height: 100vh;
  margin-top: 120px;
  max-width: 800px;
  margin-right: auto;
  margin-left: auto;
`;

const Row = styled.div`
  display: flex;
  justify-content: center;
`;

const LeaderBoardLink = styled(Link)`
  display: block;
  text-align: center;
  margin-top: 40px;
  color:#777;
  
  &:hover {
    text-decoration: none;
  }
`;

class MainPage extends React.Component {
  render() {

    return (
      <Wrapper>
        <Row>
          <DifficultySelect />
          <NameInput />
          <PlayButton />
        </Row>
        <Message />
        <PlayingField />
        <LeaderBoardLink to="/winners">Leader Board</LeaderBoardLink>
      </Wrapper>
    );
  }
}

export default MainPage;
