/* eslint-disable no-underscore-dangle */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import store from './store';

import '@babel/polyfill';
import 'reset-css/reset.css';

import MainPage from './pages/MainPage';
import LeaderBoard from './pages/LeaderBoard';




ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={MainPage} />
        <Route path="/winners" component={LeaderBoard} />
      </Switch>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root'),
);

