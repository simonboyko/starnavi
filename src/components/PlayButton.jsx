import React from 'react';
// import styled from 'styled-components';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { START_GAME, STOP_GAME, SET_MESSAGE } from '../actionsTypes';
import store from '../store';


const startGame = () => ({
  type: START_GAME,
});

const stopGame = () => ({
  type: STOP_GAME,
});

const setMessage = text => ({
  type: SET_MESSAGE,
  payload: text,
});


const mapStateToProps = state => ({
  text: state.firstGame ? 'Play' : 'Play again',
  isPlaying: state.isPlaying,
  delay: state.delay,
  username: state.username,
});

const mapDispatchToProps = (dispatch) => {
  return {
    startGame: bindActionCreators(startGame, dispatch),
    stopGame: bindActionCreators(stopGame, dispatch),
    setMessage: bindActionCreators(setMessage, dispatch),
  };
};


const Button = styled.button`
  width: 130px;
  text-transform: uppercase;
  -moz-appearance: none;
  -webkit-appearance: none;
  border-radius: 4px;
  border: none;
  cursor: pointer;
  font-size: 18px;
  background-color: #7b8d93;
  color: #fff;
  transition-duration: 0.2s;
  outline: none;
  
  &:hover {
    opacity: 0.7;
  }

`;


class PlayButton extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      firstGame: true,
    };
  }

  start() {
    if (this.state.firstGame) {
      this.setState({ firstGame: false });
    }
    this.props.startGame();
  }

  stop() {
    this.props.stopGame();
  }

  clickHandler(e) {
    if (!this.isValidation()) {
      return;
    }
    if (this.props.isPlaying) {
      this.stop();
    } else {
      this.start();
    }
  }

  isValidation() {
    if (!this.props.delay && !this.props.username) {
      this.props.setMessage('Choose a game mode and enter your name!');
      return false;
    } else if (!this.props.delay) {
      this.props.setMessage('Choose a game mode!');
      return false;
    } else if (!this.props.username) {
      this.props.setMessage('Enter your name!');
      return false;
    } else {
      return true;
    }
  }

  render() {
    let text = 'Play';
    if (this.props.isPlaying) {
      text = 'Stop';
    } else if (!this.state.firstGame) {
      text = 'Play again';
    }
    return (
      <Button
        onClick={this.clickHandler.bind(this)}
      >{text}</Button>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlayButton);
