import React from 'react';
// import styled from 'styled-components';
import axios from 'axios';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Select from 'react-select';
import { SET_FIELDS, SET_DELAY, STOP_GAME } from '../actionsTypes';
import { fetchModes } from '../api';


const setFields = (fields) => {
  return {
    type: SET_FIELDS,
    payload: fields,
  };
};

const setDelay = (delay) => {
  return {
    type: SET_DELAY,
    payload: delay,
  };
};

const stopGame = () => {
  return {
    type: STOP_GAME,
  };
};

const mapStateToProps = state => ({
  isPlaying: state.isPlaying,
});

const mapDispatchToProps = dispatch => ({
  setFields: bindActionCreators(setFields, dispatch),
  setDelay: bindActionCreators(setDelay, dispatch),
  stopGame: bindActionCreators(stopGame, dispatch),
});

const customStyles = {
  container: provider => ({
    ...provider,
    width: 240,
    marginRight: 10,
    fontSize: '18px',
    fontFamily: 'sans-serif',
  }),
  control: provider => ({
    ...provider,
    minHeight: 50,
    border: 'none',
    backgroundColor: '#cfd8dc',
    color: '#7e8f95',
  }),
  valueContainer: () => ({
    padding: '10px 20px',
  }),
  indicatorSeparator: () => ({}),
  indicatorsContainer: provider => ({
    ...provider,
    color: '#949596',
  }),
  dropdownIndicator: provider => ({
    ...provider,
    color: '#949596',
  }),
};

class DifficultySelect extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modes: null,
    };

    this.changeHandler = this.changeHandler.bind(this);
  }

  componentDidMount() {
    fetchModes()
      .then((modes) => {
        this.setState({ modes });
      });
  }

  changeHandler(selectedOption) {
    const currentModeOptions = this.state.modes[selectedOption.value];
    this.props.setFields(currentModeOptions.field);
    this.props.setDelay(currentModeOptions.delay);

    if (this.props.isPlaying) {
      this.props.stopGame();
    }
  }

  render() {
    let options = [];
    if (this.state.modes) {
      Object.keys(this.state.modes).map((modeName) => {
        options.push({
          value: modeName,
          label: modeName,
        });
      });
    }

    return (
      <Select
        onChange={this.changeHandler}
        options={options}
        placeholder={'Pick game mode'}
        styles={customStyles}
      />
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DifficultySelect);
