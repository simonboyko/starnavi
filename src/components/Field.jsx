import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';


const mapStateToProps = state => ({
  isPlaying: state.isPlaying,
});


const F = styled.div`
  line-height: 0;
  border: 1px solid #ccc;
  box-sizing: border-box;
  margin-right: -1px;
  margin-bottom: -1px;
  
  background-color: ${(props) => {
    switch (props.state) {
      case 'base':
        return '#fff';
      case 'highlighted':
        return '#42d8e8';
      case 'won':
        return '#00e871';
      case 'lost':
        return '#e85a5f';
      default:
        return '#fff';
    }
  }};
`;

class Field extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      state: null,
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.state !== prevProps.state) {
      this.setState({ state: this.props.state});
    }
  }

  setAsWon() {
    const wonEvent = new CustomEvent('won', {detail: this.props.id});
    document.dispatchEvent(wonEvent);
  }

  clickHandler(e) {
    if (this.props.state === 'highlighted' && this.props.isPlaying) {
      this.setAsWon();
    }
  }

  render() {
    return (<F
      state={this.state.state ? this.state.state : this.props.state}
      onClick={this.clickHandler.bind(this)}
    />);
  }
}


export default connect(mapStateToProps)(Field);
