import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { STOP_GAME } from '../actionsTypes';

import Field from '../components/Field';


const mapStateToProps = state => ({
  fields: state.fields,
  delay: state.delay,
});

const stopGame = winner => ({
  type: STOP_GAME,
  payload: winner,
});

const mapDispatchToProps = dispatch => ({
  stopGame: bindActionCreators(stopGame, dispatch),
});


const Fields = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: ${props => `${props.size * 50}px`};
  margin-right: auto;
  margin-left: auto;
  margin-top: 50px;
  
  > div {
    width: calc(100% / ${props => props.size});
    padding-top: calc(100% / ${props => props.size});
  }
`;

class PlayingField extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      lastFieldId: null,
      unusedFields: [],
      fields: {},
      score: {
        user: 0,
        computer: 0,
      },
    };
    this.timer = null;
    document.addEventListener('play', () => {
      this.start();
    });
    document.addEventListener('stop', () => {
      this.stopTimer();
    });
    document.addEventListener('won', this.setWon.bind(this));
  }

  componentDidMount() {
    this.updateState();
  }

  componentDidUpdate(prevProps) {
    if (this.props.fields !== prevProps.fields) {
      this.setState({ fields: {}});
      this.updateState();
    }
  }

  fieldsQuantity() {
    return this.props.fields * this.props.fields;
  }

  start() {
    this.timer = setInterval(() => {
      this.tick();
    }, this.props.delay);
    this.reset();
    this.updateState();
  }

  stopTimer() {
    clearInterval(this.timer);
  }

  stop(winner) {
    this.props.stopGame(winner);
  }

  tick() {
    const random = this.getRandomField();
    let activeFieldId;
    let newFields = { ...this.state.fields };

    if (random !== -1) {
      activeFieldId = this.state.unusedFields[random];
      newFields = {
        ...newFields,
        [activeFieldId]: { state: 'highlighted' },
      };
    }


    // mark lost field as lost
    if (this.state.lastFieldId !== null) {
      if (newFields[this.state.lastFieldId].state === 'highlighted') {
        this.state.fields[this.state.lastFieldId].state = 'lost';
        this.addScore('computer');
      }
    }

    const newUnusedFileds = this.state.unusedFields;
    newUnusedFileds.splice(random, 1);

    this.setState({
      lastFieldId: activeFieldId,
      fields: newFields,
      unusedFields: newUnusedFileds,
    });

  }

  addScore(player) {
    const newScore = this.state.score;
    switch (player) {
      case 'user':
        newScore.user += 1;
        break;
      case 'computer':
        newScore.computer += 1;
        break;
    }
    this.setState({ score: newScore });
    this.checkIfWinOrLost();
  }

  checkIfWinOrLost() {
    const theHalf = this.fieldsQuantity() / 2;
    const userScore = this.state.score.user;
    const computerScore = this.state.score.computer;
    let winner = null;

    if (userScore > theHalf) {
      winner = 'user';
    } else if (computerScore > theHalf) {
      winner = 'computer';
    } else if ((userScore === theHalf) && (computerScore === theHalf)) {
      winner = 'draw';
    }

    if (winner) {
      this.stop(winner);
    }
  }

  getRandomField() {
    const min = 0;
    const max = this.state.unusedFields.length;

    if (max === 0) {
      return -1;
    }

    let random = min + Math.random() * (max - min);
    random = Math.floor(random);
    return random;
  }

  updateState() {
    const fields = {};
    const unusedFields = [];
    for (let i = 0; i < this.fieldsQuantity(); i++) {
      fields[i] = {
        state: 'base',
      };
      unusedFields.push(i);
    };
    this.setState({
      fields: fields,
      unusedFields: unusedFields,
    });

  }

  setWon(e) {
    const fieldId = e.detail;
    const fields = { ...this.state.fields, [fieldId]: {state: 'won'}};
    this.setState({fields: fields});
    this.addScore('user');
  }

  reset() {
    this.setState({
      lastFieldId: null,
      unusedFields: [],
      fields: {},
      score: {
        user: 0,
        computer: 0,
      },
    });
  }

  render() {
    const fields = [];
    Object.keys(this.state.fields).forEach((index) => {
      const field = this.state.fields[index];
      fields.push(<Field
        state={field.state}
        id={index}
        key={index}
      />);
    });
    // this.state.fields.forEach((field) => {
    //   fields.push(<Field
    //     state={field.state}
    //     key={field.id}
    //   />);
    // });

    return (
      <Fields
        size={this.props.fields}
      >{fields}</Fields>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlayingField);
