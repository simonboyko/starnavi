import React from 'react';
import { connect} from 'react-redux';
import styled from 'styled-components';


const mapStateToProps = state => ({
  text: state.message
});


const Msg = styled.div`
  text-align: center;
  margin-top: 40px;
  color: #888888;
  font-weight: 700;
  font-size: 26px;
`;

class Message extends React.Component {
  render() {
    return (
      <Msg>{this.props.text}</Msg>
    )
  }
};


export default connect(mapStateToProps)(Message);
