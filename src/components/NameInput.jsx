import React from 'react';
// import styled from 'styled-components';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { SET_NAME } from '../actionsTypes';


const setName = (name) => {
  return {
    type: SET_NAME,
    payload: name,
  };
};


const mapStateToProps = state => ({
  name: state.username,
});

const mapDispatchToProps = (dispatch) => {
  return {
    setName: bindActionCreators(setName, dispatch),
  };
};


const Input = styled.input`
  width: 250px;
  margin-right: 10px;
  padding: 10px 20px;
  font-size: 18px;
  font-family: sans-serif;
  color: #809297;
  border: none;
  background-color: #f3f3f3;
  border-radius: 4px;
  box-sizing: border-box;
  outline: none;
`;

class NameInput extends React.Component {

  inputHandler(e) {
    this.props.setName(e.target.value);
  }

  render() {
    return (
      <Input
        type="text"
        placeholder="Enter your name"
        value={this.props.name}
        onChange={this.inputHandler.bind(this)}
      />
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NameInput);
