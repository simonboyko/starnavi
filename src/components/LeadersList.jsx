import React from 'react';
import styled from 'styled-components';
import { getWinners } from '../api';

const List = styled.ul`
  padding-left: 0;
  margin: 0;
  list-style-type: none;
  font-family: 'sans-serif';
  font-size: 18px;
`;

const Item = styled.li`
  display: flex;
  justify-content: space-between;
  align-items: center;
  color: #a1aaad;
  background-color: #cfd8dc;
  margin-bottom: 3px;
  padding: 15px 20px;
`;

class LeadersList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      list: [],
    };
  }

  componentDidMount() {
    getWinners()
      .then(res => {
        this.setState({ list: res });
      });
  }

  render() {
    const items = [];
    Array.prototype.map.call(this.state.list, (item) => {
      items.push(<Item key={item.id}>
        <div>{item.winner}</div>
        <div>{item.date}</div>
      </Item>);
    });
    return (
      <List>{items}</List>
    );
  }
};

export default LeadersList;
