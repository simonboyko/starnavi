import {
  SET_FIELDS,
  SET_DELAY,
  SET_NAME,
  START_GAME,
  STOP_GAME,
  SET_MESSAGE,
} from '../actionsTypes';
import { sendResult } from '../api';


const initialState = {
  username: '',
  fields: 5,
  delay: 0,
  isPlaying: false,
  message: 'Let\'s play!',
};


const rootReducer = (state = initialState, action) => {
  // eslint-disable-next-line default-case
  switch (action.type) {
    case SET_FIELDS:
      return { ...state, fields: action.payload };

    case SET_DELAY:
      return { ...state, delay: action.payload };

    case SET_NAME:
      return { ...state, username: action.payload };

    case STOP_GAME:
      console.log('f: stop game');

      let msg = 'Let\'s play!';
      let winner = '';
      if (action.payload === 'computer') {
        msg = 'Computer win!';
        winner = 'Computer';
      } else if (action.payload === 'user' && state.username) {
        msg = `${state.username} win!`;
        winner = state.username;
      } else if (action.payload === 'draw') {
        msg = 'Draw!';
        winner = 'Draw';
      } else {
        msg = 'Let\'s play';
      }

      if (winner) {
        sendResult(winner);
      }

      const evStop = new Event('stop');
      document.dispatchEvent(evStop);

      return {
        ...state,
        isPlaying: false,
        message: msg,
      };

    case START_GAME:
      const evPlay = new Event('play');
      document.dispatchEvent(evPlay);

      console.log('f: start game');

      return {
        ...state,
        isPlaying: true,
        firstGame: false,
        message: 'Game start!',
      };

    case SET_MESSAGE:
      return { ...state, message: action.payload };
  }
  return state;
};

export default rootReducer;
