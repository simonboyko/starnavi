import axios from 'axios/index';

const fetchModes = () => {
  return new Promise((res, rej) => {
    axios.get('http://starnavi-frontend-test-task.herokuapp.com/game-settings')
      .then(response => {
        res(response.data);
      })
      .catch(err => {
        rej(err);
      });
  });
};

const sendResult = (winner) => {
  const now = new Date();
  // 12:30; 29 May 2019
  const h = now.getHours();
  const m = now.getMinutes();
  const d = now.getDate();
  const M = now.toLocaleString('en-US', {month: 'short'});
  const y = now.getFullYear();
  const date = `${h}:${m}; ${d} ${M} ${y}`;
  axios.post('http://starnavi-frontend-test-task.herokuapp.com/winners', {
    winner: winner,
    date: date,
  });
};

const getWinners = () => {
  return new Promise((res, rej) => {
    axios('http://starnavi-frontend-test-task.herokuapp.com/winners')
      .then(response => {
        res(response.data);
      })
      .catch(err => {
        rej(err);
      });
  });
};


export { fetchModes, sendResult, getWinners };
